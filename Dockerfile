FROM centos/perl-524-centos7

LABEL maintainer="Derrek Croney <derrek.croney@duke.edu>"

LABEL io.k8s.description="Dancer2 Example with Database Support" \
  io.k8s.display-name="Dancer2 Example" \
  io.openshift.expose-services="8080:http" \
  io.openshift.tags="perl,apache,webserver,dancer" \
  # this label tells s2i where to find its mandatory scripts
  # (run, assemble, save-artifacts)
  io.openshift.s2i.scripts-url="image:///usr/libexec/s2i" \
  io.s2i.scripts-url="image:///usr/libexec/s2i" \
  usage="s2i build <SOURCE-REPOSITORY> dancer2base:latest <APP-NAME>"

USER root

RUN yum install -y openssl-devel && \
    yum install -y mariadb-devel && \
    yum clean all

USER 1001

COPY ./s2i/bin/ /usr/libexec/s2i
# ENTRYPOINT /usr/local/bin/starman -l 127.0.0.1:8080 --workers 5 -E development -I/srv/web /srv/web/bin/app.psgi
