requires "Dancer2" => "0.160001";
requires "Dancer2::Plugin::Database" => "0";
requires 'DBD::mysql', '>= 4.050';
requires "DateTime";
requires "DateTime::Set";
requires "DateTime::Format::MySQL";
requires "Dancer2::Plugin::DBIC";
requires "Dancer2::Plugin::Database";
requires "Dancer2::Plugin::Deferred";
requires "DBIx::Class::ResultSet";
requires "DBIx::Class::Result::Validation";
requires "Number::Format";
requires "YAML::XS" => "0";
requires "Template" => "0";

recommends "URL::Encode::XS"  => "0";
recommends "CGI::Deurl::XS"   => "0";
recommends "HTTP::Parser::XS" => "0";

recommends "Plack::Handler::Apache2" => "0";

on "test" => sub {
    requires "Test::More"            => "0";
    requires "HTTP::Request::Common" => "0";
};
